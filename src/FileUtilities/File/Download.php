<?php

namespace FileUtilities\File;
use \Exception;

/**
 * This class will handle anything file related and ensure all path conform to the operating system's directory separator structures
 * @category   CSI Users
 * @package    File
 */
class Download extends File {
    
    /**
     * Executes a HTTP File download stream
     * @param string|File $downloadAs
     * @param string $source
     * @throws Exception
     */
    public function download($downloadAs = null, $source = null)
    {
        if( $downloadAs instanceof File )
        {
            $source = $downloadAs->getRealPath();
            $sourceSize = $downloadAs->getSize();
            $downloadAs = $downloadAs->getFilename();
        }
        else
        {
            $source = $this->getRealPath();
            $sourceSize = $this->getSize();
            $downloadAs = $this->getFilename();
        }

        if( $source == null || $downloadAs == null )
        {
            throw new Exception('Could not download file because either the source or the filename was null:'
                                . ' Source: ' . $source
                                . ' Download As: ' . $downloadAs);
        }

        $headers   = array(
                        'Content-Type'          => 'application/force-download',
                        'Pragma'                => 'cache',
                        'Cache-Control'         => 'public, must-revalidate, max-age=0',
                        'Accept-Ranges'         => 'bytes',
                        'X-Sent-By'             => 'PEAR::HTTP::Download',
                        'Content-Disposition'   => 'attachment; filename="'.$downloadAs.'"',
                        'File'                  => $downloadAs,
                        'Content-Length'        => $sourceSize);

        foreach( $headers as $key => $value )
        {
            header($key . ":" . $value);
        }

        readfile( $source );
        exit;
    }
    
    /**
     * Streams a file down on demand. Does not have to exist
     * @param string $fileContents
     * @param string $type
     */
    public function stream($fileContents, $type = 'text')
    {
        $filename = $this->getFilename();
        
        header("Content-type: application/$type");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");
        
        echo $fileContents;
        exit;
    }
}