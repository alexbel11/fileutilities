<?php
namespace FileUtilities\Excel;

/**
 * @author Derek Miranda (derekmiranda@me.com)
 * @package CSI Users
 * @version 2.0
 * @copyright (c)2010 Communications Specialties, Inc.
 */
class Excel extends File {

    /**
     * The body contents
     * @var string
     */
    private $_body;

    /**
     * File Handler
     * @var resource
     */
    private $_handler;

    /**
     * Constructor
     * @param string $file
     */
    public function  __construct($file = 'excel.xls')
    {
        parent::__construct($file);
    }

    /**
     * Send the headers and downloads the file
     * @return headers
     */
    public function send()
    {        
        $filename = $this->getFilename();
        
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        echo $this->getContents();
        exit;
    }

    /**
     * Gets the file header
     * @return string
     */
    public function getHeader()
    {
        $header = '<html xmlns:o="urn:schemas-microsoft-com:office:office"
                            xmlns:x="urn:schemas-microsoft-com:office:excel"
                            xmlns="http://www.w3.org/TR/REC-html40">

                            <head>
                            <meta http-equiv=Content-Type content="text/html; charset=us-ascii">
                            <meta name=ProgId content=Excel.Sheet>
                            <!--[if gte mso 9]><xml>
                             <o:DocumentProperties>
                              <o:LastAuthor>Sriram</o:LastAuthor>
                              <o:LastSaved>2005-01-02T07:46:23Z</o:LastSaved>
                              <o:Version>10.2625</o:Version>
                             </o:DocumentProperties>
                             <o:OfficeDocumentSettings>
                              <o:DownloadComponents/>
                             </o:OfficeDocumentSettings>
                            </xml><![endif]-->
                            <style>
                            <!--table
                                    {mso-displayed-decimal-separator:"\.";
                                    mso-displayed-thousand-separator:"\,";}
                            @page
                                    {margin:1.0in .75in 1.0in .75in;
                                    mso-header-margin:.5in;
                                    mso-footer-margin:.5in;}
                            tr
                                    {mso-height-source:auto;}
                            col
                                    {mso-width-source:auto;}
                            br
                                    {mso-data-placement:same-cell;}
                            .style0
                                    {mso-number-format:General;
                                    text-align:general;
                                    vertical-align:bottom;
                                    white-space:nowrap;
                                    mso-rotate:0;
                                    mso-background-source:auto;
                                    mso-pattern:auto;
                                    color:windowtext;
                                    font-size:10.0pt;
                                    font-weight:400;
                                    font-style:normal;
                                    text-decoration:none;
                                    font-family:Arial;
                                    mso-generic-font-family:auto;
                                    mso-font-charset:0;
                                    border:none;
                                    mso-protection:locked visible;
                                    mso-style-name:Normal;
                                    mso-style-id:0;}
                            td
                                    {mso-style-parent:style0;
                                    padding-top:1px;
                                    padding-right:1px;
                                    padding-left:1px;
                                    mso-ignore:padding;
                                    color:windowtext;
                                    font-size:10.0pt;
                                    font-weight:400;
                                    font-style:normal;
                                    text-decoration:none;
                                    font-family:Arial;
                                    mso-generic-font-family:auto;
                                    mso-font-charset:0;
                                    mso-number-format:General;
                                    text-align:general;
                                    vertical-align:bottom;
                                    border:none;
                                    mso-background-source:auto;
                                    mso-pattern:auto;
                                    mso-protection:locked visible;
                                    white-space:nowrap;
                                    mso-rotate:0;}
                            .xl24
                                    {mso-style-parent:style0;
                                    white-space:normal;}
                            -->
                            </style>
                            <!--[if gte mso 9]><xml>
                             <x:ExcelWorkbook>
                              <x:ExcelWorksheets>
                               <x:ExcelWorksheet>
                                    <x:Name>srirmam</x:Name>
                                    <x:WorksheetOptions>
                                     <x:Selected/>
                                     <x:ProtectContents>False</x:ProtectContents>
                                     <x:ProtectObjects>False</x:ProtectObjects>
                                     <x:ProtectScenarios>False</x:ProtectScenarios>
                                    </x:WorksheetOptions>
                               </x:ExcelWorksheet>
                              </x:ExcelWorksheets>
                              <x:WindowHeight>10005</x:WindowHeight>
                              <x:WindowWidth>10005</x:WindowWidth>
                              <x:WindowTopX>120</x:WindowTopX>
                              <x:WindowTopY>135</x:WindowTopY>
                              <x:ProtectStructure>False</x:ProtectStructure>
                              <x:ProtectWindows>False</x:ProtectWindows>
                             </x:ExcelWorkbook>
                            </xml><![endif]-->
                            </head>

                            <body link=blue vlink=purple>
                            <table x:str border=0 cellpadding=0 cellspacing=0 style=\'border-collapse: collapse;table-layout:fixed;\'>';
        return $header;
    }

    /**
     * Gets the footer text
     * @return string
     */
    public function getFooter()
    {
        return '</table></body></html>';
    }

    /**
     * Sets the body content
     * @param string $string
     */
    public function setBody($string)
    {
        $this->_body = $string;
    }

    /**
     * Gets the body content
     * @return string
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * Gets the full content
     * @return string
     */
    public function getContents()
    {
        return $this->getHeader() . $this->getBody() . $this->getFooter();
    }
}