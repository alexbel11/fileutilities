<?php

namespace FileUtilities\File;
use \Exception;

class Rename extends File {

    /**
     * This will rename a file
     * From param is always derived from this objects getRealPath method
     * @param string|File $to
     * @return string
     * @throws Exception
     */
    public function rename($to)
    {
        if( ! $to instanceof File )
        {
            $to = new File($to);
        }

        $realPathOnly = new File($to->getPath());
        $fileName = $to->getFilename();
        
        if($to->exists())
        {
            // the file already exists, we need to make a custom name
            $fileName = $to->getBasename('.'. $to->getExtension()) . substr(microtime(), -5) . '.' . $to->getExtension();
        }

        if( ! rename($this->getRealPath(), $realPathOnly->getRealPath() . DIRECTORY_SEPARATOR . $fileName))
        {
            throw new Exception('Could not rename file from: ' . $this->getRealPath() . ' to: ' . $realPathOnly->getRealPath() . DIRECTORY_SEPARATOR . $fileName);
        }

        chmod($realPathOnly->getRealPath() . DIRECTORY_SEPARATOR . $fileName, 0777);
        return $to->getPath() . DIRECTORY_SEPARATOR . $fileName;
    }
}