<?php
/**
 * @author Derek Miranda (derekmiranda@me.com)
 * @package CSI Users
 * @version 2.0
 * @copyright (c)2010 Communications Specialties, Inc.
 */
namespace FileUtilities\File;

use Filter\FileSize;
use \SplFileInfo;

class File extends SplFileInfo {
   
    /**
     * File Object
     * @var SplFileObject
     */
    private $_handle;  
    
    /**
     * String of the files contents
     * @var string
     */
    private $_fileContents;

    
    /**
     * Constructor
     * @param string $file_name 
     */
    public function __construct($file_name) 
    {
        $file_name = trim($file_name);
        parent::__construct($file_name);
    }
    
    /**
     * Static function to set a prefix for all instances
     * of this object
     * @param string $prefix 
     */
    public static function setPrefix($prefix)
    {
        self::$_prefix = $prefix;
    }

    public function getSize($filter = null)
    {
        $size = parent::getSize();

        if( $filter )
        {
            $filter = new FileSize();
            $size = $filter->filter($size);
        }

        return $size;
    }
    
    /**
     * Tests if the file exists
     * @return boolean
     */
    public function exists()
    {
        return $this->isFile();
    }
    
    /**
     * Gets the extension of the file
     * @return string|null 
     */
    public function getExtension()
    {
        if( PHP_VERSION_ID >= 503060 )
        {
            return parent::getExtension();
        }
        
        $parts = explode('.', $this->getFilename());
        
        if( $parts == false || count($parts) == 0 )
        {
            return null;
        }
        
        return array_pop($parts);
    }
    
    /**
     * Gets the file object
     * @param string $open_mode
     * @return SplFileObject
     */
    public function getFileObject($open_mode = "r+")
    {
        if( $this->_handle == null )
        {
            $path = $this->getRealPath();

            if( $path === false )
            {
                $path = $this->getPath() . DIRECTORY_SEPARATOR . $this->getFilename();
            }
            
            $this->_handle = new \SplFileObject($path, $open_mode);
        }
        
        return $this->_handle;
    }
    
    /**
     * Gets an old fopen resource
     * @param string $open_mode
     * @return resource 
     */
    public function getHandle($open_mode = "r+")
    {
        return fopen($this->getRealPath(), $open_mode);
    }
    
    /**
     * Reads the contents of the file
     * @return string
     */
    public function getFileContents()
    {
        if( $this->_fileContents == null )
        {
            $handle = $this->getFileObject();

            $content = '';

            $handle->rewind();
            
            foreach($handle as $line)
            {
                $content .= $line;
            }
            
            $this->setFileContents($content);
        }
                
        return $this->_fileContents;
    }
    
    /**
     * Sets the file contents
     * @param string $contents 
     */
    public function setFileContents($contents)
    {
        $this->_fileContents = $contents;
    }

    /**
     * Gets the file name without the extension
     * @return string
     */
    public function getFilenameWithoutExtension()
    {
        return $this->getBasename('.'.$this->getExtension());
    }
    
    /**
     * Writes the contents to the file
     * @param string $contents
     * @param boolean $closeHandle
     * @return boolean
     */
    public function write($contents = null, $closeHandle = true)
    {
        if( $contents == null )
        {
            $contents = $this->getFileContents();
        }
        
        $handle = $this->getFileObject();
        $handle->rewind();
        $res = $handle->fwrite($contents);
        
        if( $closeHandle )
        {
            $this->_handle = null;
        }
        
        return $res;
    }
    
    /**
     * Deletes a file
     * @return boolean
     */
    public function delete()
    {
        return unlink($this->getRealPath());
    }
}