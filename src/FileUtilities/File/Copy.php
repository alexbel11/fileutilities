<?php

namespace FileUtilities\File;

use \Exception;

class Copy extends File {

    /**
     * This will rename a file
     * @param string $to
     * @return string
     * @throws Exception
     */
    public function copy($to)
    {
        if( ! $to instanceof File )
        {
            $to = new File($to);
        }

        $realPathOnly = new File($to->getPath());
        $fileName = $to->getFilename();
        
        if($to->exists())
        {
            // the file already exists, we need to make a custom name
            $fileName = $to->getBasename('.'. $to->getExtension()) . substr(microtime(), -5) . '.' . $to->getExtension();
        }

        if( ! copy($this->getRealPath(), $realPathOnly->getRealPath() . DIRECTORY_SEPARATOR . $fileName))
        {
            throw new Exception('Could not copy file from: ' . $this->getRealPath() . ' to: ' . $realPathOnly->getRealPath() . DIRECTORY_SEPARATOR . $fileName);
        }

        chmod($realPathOnly->getRealPath() . DIRECTORY_SEPARATOR . $fileName, 0777);
        return $to;
    }
}